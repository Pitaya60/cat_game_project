
# Cat_Ghost_Game

You play as a ghost cat who protects his master from ghosts. You destroy them with the help of spheres.

## Screenshots

![App Screenshot](https://gitlab.com/Pitaya60/cat_ghost/-/raw/main/Picture_Game.png)


## How to Play?

- **HOW TO WALK FOR A CHARACTER?** 
Use the left and right arrows 
- **HOW TO SHOOT GHOSTS?** 
Press the space bar

## About the Project
My first game is "GHOST CAT".

I write my game with Python and library PyGame

It shows all the main functions and will change in the future.

Sprites and sphere are drawn in Pixelart, and the background and sounds are taken from open sources
While there are small bugs and plans (If I have enough time, I will change the game from a bullet to a TOP-DOWN game)

- Link to the drawing site: https://www.pixilart.com/
- Link to the background music: https://youtu.be/Emt_FQFuR1A 
- Link to the background image: https://itproger.com/img/news/1589896354.jpg
- Link to the PyGame Tutorial: https://www.pygame.org/wiki/tutorials

## How to launch a project?
- Download the file 
- Find the file main.py
- Run the file using RUN 
- Play the game with pleasure)
